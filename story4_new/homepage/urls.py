from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.landing, name='landing'),
    path('gallery/', views.gallery, name='gallery'),
    # dilanjutkan ...
]